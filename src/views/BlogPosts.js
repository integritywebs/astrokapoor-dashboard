/* eslint jsx-a11y/anchor-is-valid: 0 */

import React from "react";
import {
  Container,
  Row,
  Col,
  Card,
  CardBody
} from "shards-react";

import PageTitle from "../components/common/PageTitle";

class BlogPosts extends React.Component {
  constructor(props) {
    super(props);
  this.state ={
    blogs:[]
  }
  this.delete = this.delete.bind(this);


}
  componentDidMount(){
    fetch('http://localhost:8080/api/getAllBlogs')
    .then((response) => response.json())
    .then(res => {
        this.setState({ blogs: res });
    });
  };

  delete=(id)=>{
    fetch(`http://localhost:8080/api/deleteblog/${id}`,{ method: 'DELETE' })
    .then(res => {
      console.log(res);
      console.log(res['status']);
      if(res['status']=="200"){
      alert("successfully deleted");
      window.location.reload(false);
      }else{
        alert("api failed");
      }
    });

  }
   getDate=(date)=> {
    var options = { year: 'numeric', month: 'long', day: 'numeric' };
    return new Date(date).toLocaleDateString([],options);
  }

  render() {
    return (
      <Container fluid className="main-content-container px-4">
        {/* Page Header */}
        <Row noGutters className="page-header py-4">
          <PageTitle sm="4" title="Blog Posts" subtitle="Components" className="text-sm-left" />
        </Row>

        {/* First Row of Posts */}
        <Row>
          {this.state.blogs.map((post, idx) => (
            <Col lg="3" md="6" sm="12" className="mb-4" key={idx}>
              <Card small className="card-post card-post--1">
                <div
                  className="card-post__image"
                  style={{ backgroundImage: `url(${`http://localhost:8080/api/getphoto/${post._id}`})` }}
                >
                  <div className="blog-icons" style={{padding:"10px", cursor:"pointer"}} >
                 <i className="fa fa-trash"  style={{color: "#007bff",float: "right", marginRight: "5px"}} onClick={this.delete.bind(this,post._id)}></i>
                  </div>
                </div>
                <CardBody>
                  <h5 className="card-title">
                    <a href="#" className="text-fiord-blue">
                      {post.Title}
                    </a>
                  </h5>
                  <p className="card-text d-inline-block mb-3">{post.Content}</p>
                  <div>                  <span className="text-muted">{this.getDate(post.createdAt)}</span></div>
                </CardBody>
              </Card>
            </Col>
          ))}
        </Row>
      </Container>
    );
  }
}

export default BlogPosts;
