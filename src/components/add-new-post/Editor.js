import React from "react";
import { Card, CardBody, Form, FormInput } from "shards-react";
import {useState} from 'react';
import "react-quill/dist/quill.snow.css";
import "../../assets/quill.css";
var FormData = require('form-data');

const Editor = () => {
  const[data,setData]=useState({
    title:"",
    content:"",

  })
  const [blogfile, setBlogFiles]=useState();
  
  function submit(e){
    e.preventDefault()
      var formdata = new FormData();
      formdata.append("Title", data.title);
      formdata.append("Content", data.content);
      formdata.append("Img",blogfile);

      var requestOptions = {
        method: 'POST',
        body: formdata,
        mode: 'no-cors',
        redirect: 'follow'
      };

      fetch("http://localhost:8080/api/create/blog", requestOptions)
      .then(response => response.text())
      .then(res => {
          window.location.reload(false);
      });
    
        
  }

  function handle(e){
    
    const newdata={...data}
    newdata[e.target.id]=e.target.value
    setData(newdata)
  }
  function onFileChange(e){
      setBlogFiles(e.target.files[0])
    };
  

return(
  <Card small className="mb-3">
    <CardBody>
      <Form onSubmit={(e)=>submit(e)} className="add-new-post" action=""  method="POST">
        <FormInput onChange={(e)=>handle(e)} id="title" value={data.title} size="lg" className="mb-3" placeholder="Your Post Title" />
       <textarea className="mb-3 form-control" id="content" onChange={(e)=>handle(e)} placeholder="Write Content here" style={{width: "100%", minHeight: "300px"}}></textarea>
        {/* <ReactQuill onChange={(e)=>handle(e)} id="content" value={data.content} className="add-new-post__editor mb-1" placeholder="Write Content here" /> */}
        <input type="file" onChange={(e)=>onFileChange(e)} id="blog-images"/>
        <button   className="ml-auto btn btn-accent btn-sm" type="submit"><i className="material-icons">file_copy</i> Publish</button>
      </Form>
    </CardBody>
  </Card>
)};

export default Editor;

